package com.example.dionysos



import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockkClass
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import java.lang.Exception


/**
 * The annotation specifies that any Mock or setup dependencies only
 * shall be instantiated once.
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MessageParserTest {


    /**
     * The inner class represent the function i MessageParser.kt
     * In this case there is only one function in the MessageParser class
     * and therefore it could seem trivial. However it's a good practice if
     * we always structure our test files this way.
     */
    @Nested
    inner class Parse {

        @MockK
        lateinit var cmdModel: CommandModel
        @MockK
        lateinit var parseException: ParseException

        @Test
        fun `Check if a commandModel with clock-in "properties" works`() {
            // Arrange (Given)
            cmdModel = mockkClass(CommandModel::class)


            // Act (that)
            every { cmdModel.eventType } returns CLOCK
            every { cmdModel.value } returns 1

            // Assert (then)
            assertEquals(CLOCK, cmdModel.eventType)
            assertEquals(1, cmdModel.value)
        }

        @Test
        fun `Check if a commandModel with radiation properties works`() {
            // Given
            cmdModel = mockkClass(CommandModel::class)

            // That
            every { cmdModel.eventType } returns RADIATION
            every { cmdModel.value } returns 100

            // Then
            assertEquals(RADIATION, cmdModel.eventType)
            assertEquals(100, cmdModel.value)
        }

        @Test
        fun `Check if a commandModel with clothes properties works`() {
            // Given
            cmdModel = mockkClass(CommandModel::class)

            // That
            every { cmdModel.eventType } returns CLOTHES
            every { cmdModel.value } returns 0

            // Then
            assertEquals(CLOTHES, cmdModel.eventType)
            assertEquals(0, cmdModel.value)
        }

        @Test
        fun `Check if a commandModel with warning properties works`() {
            // Given
            cmdModel = mockkClass(CommandModel::class)

            // That
            every { cmdModel.eventType } returns SYSTEM_WIDE_WARNING
            every { cmdModel.value } returns 1

            // Then
            assertEquals(SYSTEM_WIDE_WARNING, cmdModel.eventType)
            assertEquals(1, cmdModel.value)
        }

        @Test
        fun `Parse function should throw the ParseException with message "Missing $"`() {
            // Given
            val sutString = "PleaseCrash"

            // That
            val exception = Assertions.assertThrows(ParseException::class.java) {
                MessageParser.parse(sutString)
            }

            // Then
            assertEquals("Missing $", exception.message)

        }

        @Test
        fun `Parse function should throw ParseException with message "Missing colon`() {
            // Given
            val sutString = "$+PLeaseCrash"

            // That
            val exception = Assertions.assertThrows(ParseException::class.java) {
                MessageParser.parse(sutString)
            }

            // Then
            assertEquals("Missing :", exception.message)
        }

    }
}
