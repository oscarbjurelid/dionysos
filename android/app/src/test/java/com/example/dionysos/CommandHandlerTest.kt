package com.example.dionysos

import android.content.Context
import android.content.Intent
import io.mockk.*
import io.mockk.impl.annotations.MockK
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CommandHandlerTest {

    @Nested
    inner class Handle {

        @MockK
        lateinit var intent1: Intent
        lateinit var intent2: Intent

        @MockK
        lateinit var mockCommandModel: CommandModel

        @Test
        fun `Check if the intent contains our constants CLOCK and RADIATION`() {
            // Given
            intent1 = mockkClass(Intent::class)
            intent2 = mockkClass(Intent::class)

            // That
            every { intent1.action } returns ACTION_CLOCK
            every { intent1.getIntExtra("VALUE", -1) } returns 1

            every { intent2.action } returns ACTION_RADIATION
            every { intent2.getIntExtra("VALUE", -1) } returns 100

            // Then
            assertEquals(ACTION_CLOCK, intent1.action)
            assertEquals(1, intent1.getIntExtra("VALUE", -1))

            assertEquals(ACTION_RADIATION, intent2.action)
            assertEquals(100, intent2.getIntExtra("VALUE", -1))

        }

        @Test
        fun `handle should take in a commandmodel with value=1, eventType=WARNING`() {
            mockCommandModel = mockkClass(CommandModel::class)

            every { mockCommandModel.eventType } returns WARNING
            every { mockCommandModel.value } returns 1

            assertEquals(1, mockCommandModel.value)
            assertEquals(WARNING, mockCommandModel.eventType)
        }
    }
}