package com.example.dionysos

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test


class UtilitiesTest {

    /**
     * HUMAN_RAD_LIMIT is a constant from Constants.kt
     * so test results may vary.
     */
    @Test
    fun `calcTimeLeftInMillis should return 5 000 seconds`() {
        // Arrange (Given)
        val currentRadiation = 100
        val room = 1.0
        val clothes = 1

        // Act (That)
        val timeLeftInSeconds = Utilities.calcTimeLeftInSeconds(currentRadiation, room, clothes)

        // Assert (Then)
        assertEquals(50000, timeLeftInSeconds)
    }
}

