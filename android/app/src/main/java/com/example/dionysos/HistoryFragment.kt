package com.example.dionysos

import android.opengl.Visibility
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.*

/**
 * A simple [Fragment] subclass.
 */
class HistoryFragment : Fragment() {

    //region WIDGETS
    private lateinit var rv_history_list: RecyclerView
    private lateinit var tv_totalHors: TextView
    //endregion

    //region MEMBERS
    private lateinit var mView: View
    lateinit var mDBHandler: DBHandler



    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_history, container, false)
        getDBData()

        return mView
    }

    //region PRIVATE FUNCTIONS
    private fun getDBData() {
        mDBHandler = DBHandler()

        tv_totalHors = mView.findViewById(R.id.totalTime)

        if (tag == DYATLOV.toString()) {
            mDBHandler.getHistory { success, listModels ->
                if (!success)
                    showError()
                else {
                    initHistoryList(listModels!!)
                    tv_totalHors.visibility = View.GONE
                }
            }
        }
        else {
            mDBHandler.getEmployeeHistory(tag!!) {success, listModels ->
                if(!success)
                    showError()
                else {
                    initHistoryList(listModels!!)

                    tv_totalHors.visibility = View.VISIBLE

                    mDBHandler.getEmployeeHoursAndRadiation(tag!!) { success, hours, rad ->
                        if (!success)
                            Log.w("HISTORY FRAGMENT", "Failed fetch from Firestore")
                        else {

                            tv_totalHors.text = "HOURS(SECONDS): $hours\nRADIATION: $rad"
                        }
                    }



                }
            }
        }

    }




    private fun initHistoryList(list: ArrayList<ListModel>) {
        rv_history_list = mView.findViewById<RecyclerView>(R.id.rv_history_list).apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(mView.context)
            adapter = HistoryAdapter(list)

        }
    }




    private fun showError() {
        activity!!.supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_history, ErrorFragment(), " ")
            .addToBackStack(null)
            .commit()
    }
    //endregion


}
