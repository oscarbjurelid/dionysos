package com.example.dionysos

import android.content.Context
import android.content.Intent
import android.util.Log
import java.time.Duration
import java.time.LocalTime

/**
 * [CommandHandler] delegates incoming messages in their correct direction
 * @param mContext: Needed to send intents to [MainActivity.BroadcastReceiver1]
 */
class CommandHandler(private val mContext: Context) {

    //region MEMBER VARIABLES
    private val mDBHandler = DBHandler()
    //endregion

    //region PUBLIC FUNCTIONS
    fun handle(cmdModel: CommandModel) {

        val intent = Intent()

        when (cmdModel.eventType) {

            CLOCK -> {
                mDBHandler.writeToLog(cmdModel)

                intent.action = ACTION_CLOCK
                intent.putExtra("VALUE", cmdModel.value)

                /* Set startTime in ExposureHandler */
                if (cmdModel.value == IN)
                    ExposureHandler.setStartTime()
                if (cmdModel.value == OUT) {
                    val elapsedTime =  Duration.between(TechnicianModel.shiftStart, LocalTime.now()).seconds
                    var lastTime = 0
                    mDBHandler.getEmployeeHoursAndRadiation(AKIMOV.toString()) { success, hours, rad ->
                        if(!success)
                            Log.e("CommandHandler", "Firebase failed fetch of hours")

                        else {
                            lastTime = hours!!
                            val totalTime = lastTime + elapsedTime
                            val exposedRad = HUMAN_RAD_LIMIT - TechnicianModel.exposureLeft + rad!!
                            mDBHandler.updateEmployeeData(AKIMOV.toString(), totalTime.toInt(), exposedRad) { success ->
                                if (!success)
                                    Log.e("CommandHandler", "Firebase failed to update")
                            }
                        }
                    }
                }

            }
            RADIATION -> {
                SafetyConsoleModel.radiationLevel = cmdModel.value
                ExposureHandler.updateExposureLimit(LocalTime.now())

                val timeLeft = Utilities.calcTimeLeftInSeconds()
                mDBHandler.writeToLog(cmdModel)

                intent.action = ACTION_RADIATION
                intent.putExtra("VALUE", timeLeft)


            }
            WARNING -> {
                mDBHandler.writeToLog(cmdModel)

                intent.action = ACTION_RECEIVE_SYSTEM_WIDE_WARNING
                intent.putExtra("VALUE", cmdModel.value)
            }
            CLOTHES -> {
                TechnicianModel.hazmatOn = cmdModel.value
                ExposureHandler.updateExposureLimit(LocalTime.now())

                mDBHandler.writeToLog(cmdModel)

                val timeLeft = Utilities.calcTimeLeftInSeconds()

                intent.action = ACTION_CLOTHES
                intent.putExtra("VALUE", cmdModel.value)
                intent.putExtra("TIMELEFT", timeLeft)


            }
            ROOM -> {
                TechnicianModel.roomIn = cmdModel.value
                ExposureHandler.updateExposureLimit(LocalTime.now())

                mDBHandler.writeToLog(cmdModel)

                val timeLeft = Utilities.calcTimeLeftInSeconds()

                intent.action = ACTION_ROOM
                intent.putExtra("VALUE", cmdModel.value)
                intent.putExtra("TIMELEFT", timeLeft)

            }

        }
        mContext.sendBroadcast(intent)
    }
    //endregion
}
