package com.example.dionysos

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import kotlin.math.roundToInt


/**
 * The [Utilities] class consists of a number of static functions to solve easy access calculations.
 * This files should only consist of static functions (companion object)
 */
class Utilities {

    companion object{

        @JvmStatic
        fun getCurrentTime(format: String = "yyyy-MM-dd HH:mm:ss.SSS") : String {

            val current = LocalDateTime.now()
            val formatter = DateTimeFormatter.ofPattern(format)

            return current.format(formatter)
        }

        /**
         * @return Time left until worker needs to leave building
         */
        @JvmStatic
        fun calcTimeLeftInSeconds(currentRadiation: Int = SafetyConsoleModel.radiationLevel,
                                  roomCoef: Double = TechnicianModel.getRoomCoefficient(),
                                  clothesCoef: Int = TechnicianModel.getClothesCoefficient()) : Int {
            return try {
                ((TechnicianModel.exposureLeft) / ((currentRadiation * roomCoef) / clothesCoef)).roundToInt()
            } catch (e: Exception) {
                // Division by zero exception
                (TechnicianModel.exposureLeft) / ((1 * roomCoef) / clothesCoef).roundToInt()
            }
        }


    }



}