package com.example.dionysos

/**
 * A simple [Exception] to clarify what kind of exception that was thrown
 */
class ParseException(message: String) : Exception(message)