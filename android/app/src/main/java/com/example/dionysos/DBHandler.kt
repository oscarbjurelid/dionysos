package com.example.dionysos

import android.util.Log
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.firestore.*
import kotlin.collections.HashMap

private val TAG = DBHandler::class.qualifiedName

/**
 * Implements a connection to the apps [com.google.firebase.firestore] instance
 * and handles all it's CRUD operations
 */
class DBHandler {

    //region MEMBER VARIABLES
    private var db: FirebaseFirestore = FirebaseFirestore.getInstance()
    //endregion

    //region PRIVATE FUNCTIONS
    /**
     * @param model
     * @return HashMap<String, Any>
     */
    private fun parse(model: CommandModel) : HashMap<String, Any> {

        var action = ""

        when(model.eventType) {
            CLOCK -> {
                action = if (model.value == IN) LOG_FIELD_VALUE_CLOCK_IN else LOG_FIELD_VALUE_CLOCK_OUT
            }
            RADIATION -> {
                action = LOG_FIELD_VALUE_RADIATION
            }
            CLOTHES -> {
                action = if (model.value == ON) LOG_FIELD_VALUE_HAZMAT_ON else LOG_FIELD_VALUE_HAZMAT_OFF
            }
            ROOM -> {
                action = LOG_FIELD_VALUE_ROOM
            }
            WARNING -> {
                action = LOG_FIELD_VALUE_WARNINGS
            }
        }

        return hashMapOf(
            LOG_FIELD_KEY_ID to AKIMOV.toString(),
            LOG_FIELD_KEY_ACTION to action,
            LOG_FIELD_KEY_TIME to Utilities.getCurrentTime(),
            LOG_FIELD_KEY_RADIATION to SafetyConsoleModel.radiationLevel,
            LOG_FIELD_KEY_ROOM to TechnicianModel.roomIn
        )
    }
    //endregion


    //region PUBLIC FUNCTIONS
    /**
     *  Writes model to database table "log"
     * @param model
     */
    fun writeToLog(model: CommandModel) {

        val event = parse(model)

        db.collection(COLLECTION_LOG)
            .document(""+event.getValue("TIME"))
            .set(event)

            .addOnSuccessListener {
                Log.d(TAG, "Document snapshot added successfully")
            }

            .addOnFailureListener{ e ->
                Log.w(TAG, "Error adding document", e)
            }
    }

    /**
     * Get hours worked by an employee with specific id
     *
     * @param employeeId
     * @callback Boolean, Int (null when failure)
     * */
    fun getEmployeeHoursAndRadiation(employeeId: String, completion: (success: Boolean, hours: Int?, rad: Int?) -> Unit) {

        val docRef = db.collection(COLLECTION_EMPLOYEES).document(employeeId)

        docRef.get()
            .addOnCompleteListener { task ->
                if(task.isSuccessful) {
                    val document = task.result
                    if(document != null) {
                        val totalHours = document.get(EMPLOYEES_FIELD_KEY_HOURS).toString().toInt()
                        val totalRad = document.get(EMPLOYEES_FIELD_KEY_RADIATION).toString().toInt()

                        completion(true, totalHours, totalRad)
                    } else {
                        Log.d(TAG, "No such doc exist:")
                    }
                } else {
                    Log.d(TAG, "query failed" + task.exception)
                    completion(false, null, null)

                }
            }
    }
    
    /**
    * Get an employee's entire log history, with specific id.
    *
    * @param employeeId
    * @callback Boolean, MutableList<HashMap<String, String>> (null when failure)
    * */
    //TODO not tested yet
    fun getEmployeeHistory(employeeId: String, completion: (success: Boolean, listModels: ArrayList<ListModel>?) -> Unit) {

        val listModels = ArrayList<ListModel>()

        val docRef = db.collection(COLLECTION_LOG)

        docRef.whereEqualTo("ID", employeeId)
            .get()
            .addOnSuccessListener { documents ->

                for(document in documents) {

                    val listModel = mapToListModel(document)
                    listModels.add(listModel)
                }
                completion(true, listModels)
            }
            .addOnFailureListener { exception ->
                Log.e("DBHandler: getIdFilteredDocuments", exception.toString())
                completion(false, null)
            }
    }

    /**
     * Get entire log history.
     *
     * @callback Boolean, MutableList<HashMap<String, String>> (null when failure)
     * */
    //TODO not tested yet
    fun getHistory(completion: (success: Boolean, listModels: ArrayList<ListModel>?) -> Unit) {

        val listModels = ArrayList<ListModel>()

        val docRef = db.collection(COLLECTION_LOG)

        docRef.get()
            .addOnSuccessListener { documents ->

                for(document in documents) {

                    val listModel = mapToListModel(document)
                    listModels.add(listModel)
                }
                completion(true, listModels)
            }
            .addOnFailureListener { exception ->
                Log.e("DBHandler: getHistory", exception.toString())
                completion(false, null)
            }
    }

    /**
     * Update an employees worked hours and amount of radiation, with specific id.
     *
     * @param employeeId, hours, radiation
     * @callback Boolean, MutableList<HashMap<String, String>> (null when failure)
     * */
    //TODO not tested yet
    fun updateEmployeeData(employeeId: String, hours: Int, radiation: Int, completion: (success: Boolean) -> Unit) {

        val docRef = db.collection(COLLECTION_EMPLOYEES).document(employeeId)

        docRef.update(mapOf(
            EMPLOYEES_FIELD_KEY_HOURS to hours,
            EMPLOYEES_FIELD_KEY_RADIATION to radiation))
            .addOnSuccessListener {
                Log.i(TAG, "updateEmployeeData: SUCCESS" )
                completion(true)
            }
            .addOnFailureListener {
                e -> Log.e(TAG, "updateEmployeeData: FAILURE", e)
                completion(false)
            }
    }

    /**
     * Converts DocumentSnap to HaspMap
     *
     * @param document
     * @return HashMap<String, String>
     * */
    private fun mapToListModel(document: DocumentSnapshot): ListModel {
        val action = document.data?.get("ACTION").toString()
        val time = document.data?.get("TIME").toString()

        val listModel = ListModel(action, time)

        when(action) {
            LOG_FIELD_VALUE_ROOM -> {
                val currentRoom = document.data?.get(LOG_FIELD_KEY_ROOM).toString()

                listModel.value = currentRoom
            }
            LOG_FIELD_VALUE_RADIATION -> {
                val radiationLevel = document.data?.get(LOG_FIELD_KEY_RADIATION).toString()

                listModel.value = radiationLevel
            }
        }

        listModel.setAction()
        return listModel
    }
    //endregion

}