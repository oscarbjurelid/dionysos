package com.example.dionysos

import android.util.Log


/**
 * [MessageParser] takes a [String] and parse it as a [CommandModel]
 */
class MessageParser {

    companion object {
        /**
         * s = $1:1$ // utopia
         * a = [0, 1:1]
         * b = [1, 1]
         *
         * @param s = Converted bytearray as string
         * @return CommandModel
         * @throws ParseException or Exception
         * */
        @JvmStatic
        fun parse(s: String): CommandModel {

            if(!s.contains("$")) throw ParseException("Missing $")
            if(!s.contains(":")) throw ParseException("Missing :")


            try {
                val a = s.split("$")
                val b = a[1].split(":")

                return CommandModel(b[0].toInt(), b[1].toInt())

            } catch (e: ParseException) {
                Log.e("MessageParser", e.toString())
                //throw (e)
            } catch (e: Exception) {
                Log.e("MessageParser", e.toString())
                //throw (e)
            }
            /* SANITY CHECK */
            return CommandModel(-1, -1)
        }

    }
}