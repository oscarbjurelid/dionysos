package com.example.dionysos

import android.content.Context
import com.afollestad.materialdialogs.MaterialDialog

/**
 * [WarningDialog] shows a Dialog filled with a specified error message from the given
 * @param title: A subject to the dialog
 * @param msg: A message who explains what went wrong
 */
class WarningDialog(context: Context, title: String, msg: String) {

    private var dialog: MaterialDialog = MaterialDialog(context)
        .title(text = title)
        .message(text = msg)
        .positiveButton(text = "OK")

    fun show() { dialog.show() }


}