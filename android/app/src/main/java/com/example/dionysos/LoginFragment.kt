package com.example.dionysos


import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import androidx.core.content.ContextCompat
import androidx.core.text.isDigitsOnly
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment


/**
 * A simple [Fragment] subclass.
 */
class LoginFragment : Fragment() {

    //region WIDGETS
    private lateinit var etLogin: EditText
    //endregion

    //region MEMBERS
    private lateinit var mView: View
    //endregion

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_login, container, false)
        initWidgets()
        initListeners()

        return mView
    }


    //region PRIVATE FUNCTIONS

    private fun initWidgets() {
        etLogin = mView.findViewById(R.id.etEmployeeID)
    }

    private fun initListeners() {
        etLogin.addTextChangedListener {
            if (etLogin.text.isNotEmpty()) {
                if(authLogin(etLogin.text.toString().toInt())) {
                    val input = etLogin.text.toString().toInt()

                    hideKeyboard()
                    activity!!.supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.fragment_login, HistoryFragment(), input.toString())
                        .addToBackStack(null)
                        .commit()
                }
            }
        }
    }

    private fun authLogin(input: Int) : Boolean {
        return EMPLOYEES.contains(input)
    }

    private fun hideKeyboard() {
        val inputMethodManager = mView.context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(mView.windowToken, 0)
    }
    //endregion


}
