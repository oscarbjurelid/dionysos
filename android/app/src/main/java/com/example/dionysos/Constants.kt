package com.example.dionysos

//region EVENT TYPES
const val CLOCK = 0
const val RADIATION = 1
const val CLOTHES = 2
const val WARNING = 4
const val ROOM = 5
//endregion

//region EVENT VALUES
const val IN = 1
const val OUT = 0

const val ON = 1
const val OFF = 0
//endregion

//region INTENT ACTIONS
const val ACTION_CLOCK = "CLOCK"
const val ACTION_RADIATION = "RADIATION"
const val ACTION_CLOTHES = "CLOTHES"
const val ACTION_WARNING_NOTIFY = "WARNING_NOTIFY"
const val ACTION_SYSTEM_WIDE_WARNING = "WARNING_SYSTEM_WIDE"
const val ACTION_RECEIVE_SYSTEM_WIDE_WARNING = "RECEIVE_SYSTEM_WIDE"
const val ACTION_ROOM = "ROOM"
//endregion

//region HUMAN RADIATION SAFETY LIMIT
const val HUMAN_RAD_LIMIT = 500000
//endregion

//region CONNECTIVITY
const val MAC_ADDRESS_SAFETY_CONSOLE: String = "98:D3:81:FD:4A:C0"
//endregion

//region INTENT
const val REQUEST_ENABLE_BT = 1
//endregion

//region SEND CONSTANTS
const val TIME_LEFT_INDICATION = 0
const val SYSTEM_WIDE_WARNING = 1

//region ROOMS
const val BREAK_ROOM = 1
const val REACTOR_ROOM = 2
const val CONTROL_ROOM = 3
//endregion

//region ROOM COEFFICIENTS
const val BREAK_ROOM_COEF = 0.1
const val REACTOR_ROOM_COEF = 1.6
const val CONTROL_ROOM_COEF = 0.5
//endregion

//region CLOTHES COEFFICIENTS
const val HAZMAT_ON_COEF = 5
const val HAZMAT_OFF_COEF = 1
//endregion


//region FIRESTORE FIELD KEYS
const val LOG_FIELD_KEY_ID = "ID"
const val LOG_FIELD_KEY_ACTION = "ACTION"
const val LOG_FIELD_KEY_TIME = "TIME"
const val LOG_FIELD_KEY_RADIATION = "RADIATION LEVEL"
const val LOG_FIELD_KEY_ROOM = "CURRENT ROOM"
const val LOG_FIELD_KEY_HAZMAT = "HAZMAT ON/OFF"
const val EMPLOYEES_FIELD_KEY_HOURS = "totalHours"
const val EMPLOYEES_FIELD_KEY_RADIATION = "totalRadiation"
//endregion

//region FIRESTORE FIELD VALUES
const val LOG_FIELD_VALUE_CLOCK_IN = "CLOCK-IN"
const val LOG_FIELD_VALUE_CLOCK_OUT = "CLOCK-OUT"
const val LOG_FIELD_VALUE_RADIATION = "RADIATION LEVEL CHANGED"
const val LOG_FIELD_VALUE_ROOM = "ROOM CHANGED"
const val LOG_FIELD_VALUE_HAZMAT_ON = "HAZMAT ON"
const val LOG_FIELD_VALUE_HAZMAT_OFF = "HAZMAT OFF"
const val LOG_FIELD_VALUE_WARNINGS = "WARNING OCCURED"
//endregion

//region FIRESTORE COLLECTIONS
const val COLLECTION_LOG = "log"
const val COLLECTION_EMPLOYEES = "employees"
//endregion

//region EMPLOYEE IDS
val EMPLOYEES = intArrayOf(999, 123)
const val DYATLOV = 999
const val AKIMOV = 123
//endregion



