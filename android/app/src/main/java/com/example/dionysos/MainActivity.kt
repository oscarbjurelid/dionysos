package com.example.dionysos


import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    //region WIDGETS
    private lateinit var tvState: TextView
    private lateinit var tvCountDownTimer: TextView
    private lateinit var ivRoomLight1On: ImageView
    private lateinit var ivRoomLight1Off: ImageView
    private lateinit var ivRoomLight2On: ImageView
    private lateinit var ivRoomLight2Off: ImageView
    private lateinit var ivRoomLight3On: ImageView
    private lateinit var ivRoomLight3Off: ImageView
    private lateinit var ivHazmatOn: ImageView
    private lateinit var ivHazmatOff: ImageView
    private lateinit var cvNeedleView: NeedleView

    //endregion

    //region FRAGMENT RELATED
    private lateinit var mLoginFragment: LoginFragment

    //region CLASS MEMBERS
    private var isConnected = false
    private val mBroadcastReceiver = BroadcastReceiver1()
    private lateinit var mBluetoothHandler: BluetoothHandler
    private lateinit var mBluetoothAdapter: BluetoothAdapter
    private lateinit var mCountDown: CountDown
    //endregion

    //region Listeners
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                supportFragmentManager
                    .beginTransaction()
                    .remove(mLoginFragment)
                    .commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_history -> {
                if (mLoginFragment.isAdded) return@OnNavigationItemSelectedListener false
                supportFragmentManager
                    .beginTransaction()
                    .add(R.id.root_layout, mLoginFragment)
                    .commit()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }
    //endregion

    //region LIFECYCLE METHODS
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // RESOLVE WIDGETS
        resolveWidgets()

        val bottomNavigation: BottomNavigationView = findViewById(R.id.navigationView)
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)


        // INIT MEMBERS
        mCountDown = CountDown(this, tvCountDownTimer)
        mLoginFragment = LoginFragment()

        setupBroadcastReceiver()
        setupBluetoothHandler()
    }

    override fun onDestroy() {
        super.onDestroy()
        mBluetoothHandler.stop()
        unregisterReceiver(mBroadcastReceiver)

    }
    //endregion

    //region PRIVATE MEMBER METHODS
    private fun resolveWidgets() {
        tvState = findViewById(R.id.tvState)
        tvCountDownTimer = findViewById(R.id.tvCountdown)

        ivRoomLight1On = findViewById(R.id.roomLight1_on)
        ivRoomLight1Off = findViewById(R.id.roomLight1_off)

        ivRoomLight2On = findViewById(R.id.roomLight2_on)
        ivRoomLight2Off = findViewById(R.id.roomLight2_off)

        ivRoomLight3On = findViewById(R.id.roomLight3_on)
        ivRoomLight3Off = findViewById(R.id.roomLight3_off)

        ivHazmatOn = findViewById(R.id.hazmatLightsOn)
        ivHazmatOff = findViewById(R.id.hazmatLightsOff)

        cvNeedleView = findViewById(R.id.needleCanvas)
    }

    private fun setupBluetoothHandler() {

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        if(!mBluetoothAdapter.isEnabled) {

            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)

        } else {

            mBluetoothHandler = BluetoothHandler(this)


            isConnected = mBluetoothHandler.init()


            if (isConnected) mBluetoothHandler.startListeningForMsg()
        }
    }

    private fun setupBroadcastReceiver() {
        val filter = IntentFilter()
        filter.addAction(ACTION_CLOCK)
        filter.addAction(ACTION_RADIATION)
        filter.addAction(ACTION_CLOTHES)
        filter.addAction(ACTION_ROOM)
        filter.addAction(ACTION_WARNING_NOTIFY)
        filter.addAction(ACTION_SYSTEM_WIDE_WARNING)
        filter.addAction(ACTION_RECEIVE_SYSTEM_WIDE_WARNING)

        registerReceiver(mBroadcastReceiver, filter)
    }
    //endregion


    //region PUBLIC MEMBER METHODS
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                setupBluetoothHandler()
            }
            else {
                Toast.makeText(this, "Please enable Bluetooth", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun updateState(type: Int) {

        when (type) {
            IN -> {

                tvState.text = resources.getText(R.string.clockIn)
            }
            OUT -> {
                tvState.text = resources.getText(R.string.clockOut)
                mCountDown.cancelTimer()
                tvCountDownTimer.text = ""

                ivRoomLight1On.visibility = View.INVISIBLE
                ivRoomLight1Off.visibility = View.VISIBLE

                ivRoomLight2On.visibility = View.INVISIBLE
                ivRoomLight2Off.visibility = View.VISIBLE

                ivRoomLight3On.visibility = View.INVISIBLE
                ivRoomLight3Off.visibility = View.VISIBLE

                ivHazmatOn.visibility = View.INVISIBLE
                ivHazmatOff.visibility = View.VISIBLE
            }
            else -> tvState.text = resources.getText(R.string.error)
        }
    }

    fun updateTimer(timeLeft: Int) {

        /* Sanity check*/
        if (timeLeft == -1) {
            tvCountDownTimer.text = resources.getText(R.string.error)
        }

        try { mCountDown.cancelTimer() }

        catch (e: UninitializedPropertyAccessException) { }

        finally {
            tvCountDownTimer.text = ""
            mCountDown = CountDown(this, tvCountDownTimer, timeLeft)
            mCountDown.startTimer()
        }
    }

    fun showWarning(timeLeft: String, title: String) {
        val warningDialog = WarningDialog(this, timeLeft, title)
        warningDialog.show()
    }

    fun toggleReceivedWarning(input: Int) {
        if(input == 1) {
            tvState.text = ""
            tvState.text = resources.getText(R.string.warning)

        } else if(input == 0){
            tvState.text = ""
        } else {
            tvState.text = ""
        }

    }

    fun onClothesChanged(clothesOn: Int) {
        if (clothesOn == 1) {
            tvState.text = resources.getText(R.string.hazmatOn)

            ivHazmatOn.visibility = View.VISIBLE
            ivHazmatOff.visibility = View.INVISIBLE
        } else {
            tvState.text = resources.getText(R.string.hazmatOff)

            ivHazmatOn.visibility = View.INVISIBLE
            ivHazmatOff.visibility = View.VISIBLE
        }

    }

    fun onRoomChanged(currentRoom: Int) {
        when (currentRoom) {
            BREAK_ROOM -> {
                tvState.text = resources.getText(R.string.breakRoom)

                ivRoomLight1Off.visibility = View.INVISIBLE
                ivRoomLight1On.visibility = View.VISIBLE

                ivRoomLight2On.visibility = View.INVISIBLE
                ivRoomLight2Off.visibility = View.VISIBLE

                ivRoomLight3On.visibility = View.INVISIBLE
                ivRoomLight3Off.visibility = View.VISIBLE
            }

            REACTOR_ROOM -> {
                tvState.text = resources.getText(R.string.reactorRoom)

                ivRoomLight1Off.visibility = View.VISIBLE
                ivRoomLight1On.visibility = View.INVISIBLE

                ivRoomLight2On.visibility = View.VISIBLE
                ivRoomLight2Off.visibility = View.INVISIBLE

                ivRoomLight3On.visibility = View.INVISIBLE
                ivRoomLight3Off.visibility = View.VISIBLE
            }
            CONTROL_ROOM -> {
                tvState.text = resources.getText(R.string.controlRoom)

                ivRoomLight1Off.visibility = View.VISIBLE
                ivRoomLight1On.visibility = View.INVISIBLE

                ivRoomLight2On.visibility = View.INVISIBLE
                ivRoomLight2Off.visibility = View.VISIBLE

                ivRoomLight3On.visibility = View.VISIBLE
                ivRoomLight3Off.visibility = View.INVISIBLE
            }

        }
    }
    //endregion

    //region INNER CLASS
    private inner class BroadcastReceiver1 : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                ACTION_CLOCK -> {
                    updateState(intent.getIntExtra("VALUE", -1))
                }
                ACTION_RADIATION -> {
                    val timeLeft = intent.getIntExtra("VALUE", -1)
                    cvNeedleView.setRadiationLevel()
                    updateTimer(timeLeft)
                    mBluetoothHandler.send(TIME_LEFT_INDICATION, timeLeft)

                }
                ACTION_WARNING_NOTIFY -> {
                    val timeLeft = intent.getStringExtra("TIME")
                    val title = intent.getStringExtra("TITLE")

                    showWarning(timeLeft!!, title!!)
                }
                ACTION_SYSTEM_WIDE_WARNING -> {
                    val value = intent.getIntExtra("VALUE", -1)
                    mBluetoothHandler.send(SYSTEM_WIDE_WARNING, value)
                }
                ACTION_RECEIVE_SYSTEM_WIDE_WARNING -> {
                    val value = intent.getIntExtra("VALUE", -1)
                    toggleReceivedWarning(value)
                }
                ACTION_CLOTHES -> {
                    val clothesOn = intent.getIntExtra("VALUE", -1)
                    val timeLeft = intent.getIntExtra("TIMELEFT", -1)

                    onClothesChanged(clothesOn)
                    updateTimer(timeLeft)
                    mBluetoothHandler.send(TIME_LEFT_INDICATION, timeLeft)

                }
                ACTION_ROOM -> {
                    val currentRoom = intent.getIntExtra("VALUE", -1)
                    val timeLeft = intent.getIntExtra("TIMELEFT", -1)

                    onRoomChanged(currentRoom)
                    updateTimer(timeLeft)

                    mBluetoothHandler.send(TIME_LEFT_INDICATION, timeLeft)
                }

            }
        }
    }
    //endregion

}