package com.example.dionysos


/**
 * A model who represent the data who shall be rendered in [HistoryFragment] via [HistoryAdapter]
 */
class ListModel(var action: String, val time: String) {

    var icon: Int? = null
    var value: String = ""

    init {
        setIcon()
    }


    private fun setIcon() {
        when (action) {
            LOG_FIELD_VALUE_CLOCK_IN -> icon = R.drawable.ic_clock_in_regular
            LOG_FIELD_VALUE_CLOCK_OUT -> icon = R.drawable.ic_clock_out_regular
            LOG_FIELD_VALUE_RADIATION -> icon = R.drawable.ic_radiation_solid
            LOG_FIELD_VALUE_HAZMAT_ON -> icon = R.drawable.ic_hazmat_on_solid
            LOG_FIELD_VALUE_HAZMAT_OFF -> icon = R.drawable.ic_hazmat_off_solid
            LOG_FIELD_VALUE_ROOM -> icon = R.drawable.ic_room_solid
            LOG_FIELD_VALUE_WARNINGS -> icon = R.drawable.ic_exclamation_triangle_solid
        }
    }

    fun setAction() {
        when(action) {
            LOG_FIELD_VALUE_RADIATION -> {
                action = "RADIATION LEVEL: $value"
            }
            LOG_FIELD_VALUE_ROOM -> {
                action = when (value) {
                    "1" -> "BREAK ROOM"
                    "2" -> "REACTOR ROOM"
                    "3" -> "CONTROL ROOM"
                    else -> "AWESOME ROOM"
                }
            }
        }
    }


}
