package com.example.dionysos

import java.time.Duration
import java.time.LocalTime

/**
 * A static instance that handles a technician exposure of radiation.
 */
class ExposureHandler {

    companion object {
        private var startTime: LocalTime = LocalTime.now()

        fun updateExposureLimit(newTime: LocalTime) {
            val diff = Duration.between(startTime, newTime).seconds

            val exposurePerSecond = calcExposurePerSecond()
            val beenExposedTo = (diff * exposurePerSecond).toInt()

            TechnicianModel.exposureLeft -= beenExposedTo

            startTime = newTime
        }

        fun setStartTime() {
            startTime = LocalTime.now()
        }

        private fun calcExposurePerSecond(): Int {
            val room = TechnicianModel.getRoomCoefficient()
            val clothes = TechnicianModel.getClothesCoefficient()
            val radiationLvl = SafetyConsoleModel.radiationLevel

            return (radiationLvl * room / clothes).toInt()
        }


    }

}

