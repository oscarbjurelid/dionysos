package com.example.dionysos

import android.content.Context
import android.content.Intent
import android.os.CountDownTimer
import android.widget.TextView

/**
 * [CountDown] inherits from [CountDownTimer] and implements a
 * timer whereas
 * @param startTime indicates for how long the timer shall count down from.
 * @param mContext is used to send intents to [MainActivity.BroadcastReceiver1]
 * @param tvTimer is used to display the return value from [CountDown.format]
 * @param interval specifies the interval the timer shall be called in milliseconds.
 */
class CountDown(private var mContext: Context,
                private var tvTimer: TextView,
                startTime: Int = -1,
                interval: Long = 1000) : CountDownTimer(startTime.toLong() * 1000, interval) {

    //region PRIVATE FUNCTIONS
    private fun format(timeLeftInMillis: Long) : String {
        val seconds = (timeLeftInMillis / (1000)) % 60
        val minutes = (timeLeftInMillis / (1000 * 60)) % 60
        val hours = (timeLeftInMillis / (1000 * 60 * 60))


        var s = seconds.toString()
        var m = minutes.toString()
        var h = hours.toString()

        if (seconds < 10)
            s = "0$seconds"
        if (minutes < 10)
            m = "0$minutes"
        if (hours < 10)
            h = "0$hours"

        if (hours > 99) return "99 : 59 : 59"

        return "$h : $m : $s"
    }

    private fun sendWarning(timeLeft: String, title: String) {
        val intent = Intent()

        intent.action = ACTION_WARNING_NOTIFY
        intent.putExtra("TIME", timeLeft)
        intent.putExtra("TITLE", title)

        mContext.sendBroadcast(intent)
    }

    private fun sendSystemWideWarning() {
        val intent = Intent()

        intent.action = ACTION_SYSTEM_WIDE_WARNING
        intent.putExtra("VALUE", 1)

        mContext.sendBroadcast(intent)
    }
    //endregion

    //region LIFE CYCLE METHODS
    override fun onFinish() {
        tvTimer.text = ""
        sendSystemWideWarning()
        cancel()
    }

    override fun onTick(millisUntilFinished: Long) {
        tvTimer.text = format(millisUntilFinished)


        if (millisUntilFinished in 29001..29999) {
            sendWarning(format(millisUntilFinished), "HURRY UP!")
        }
        if (millisUntilFinished in 14000..15000){
            sendWarning(format(millisUntilFinished), "RUN FORREST RUN")
        }
        if (millisUntilFinished in 9000..10000) {
            sendWarning(format(millisUntilFinished), "R.I.P")
        }
    }

    fun cancelTimer() { this.cancel() }

    fun startTimer() { this.start() }
    //endregion
}