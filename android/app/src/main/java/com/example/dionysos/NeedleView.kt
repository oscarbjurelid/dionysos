package com.example.dionysos

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import kotlin.math.asin
import kotlin.math.sin

/**
 * [NeedleView] represent a Becquerel meter adjust by the emitted
 * radiation from the Nuclear Power Plant
 */
class NeedleView(context: Context, attrs: AttributeSet) : View(context, attrs) {

    private val mContext: Context = context
    private var mCanvas: Canvas
    private val mPaint: Paint
    private val kValue = asin(1/340f)

    var radX = 150f
    var radY = 310f



    init {
        mCanvas = Canvas()
        mPaint = Paint()

        init()

    }

    override fun onDraw(canvas: Canvas?) {
        mCanvas = canvas!!
        super.onDraw(mCanvas)

        mCanvas.drawLine(
            radX,
            radY,
            width/2f,
            height.toFloat()/2f,
            mPaint
        )

    }

    private fun init() {
        mPaint.isAntiAlias = true
        mPaint.color = Color.BLACK
        setWillNotDraw(false)




    }

    fun setRadiationLevel() {
        val radiation = SafetyConsoleModel.radiationLevel

        val x = 150 + (6.8 * radiation)
        val y = 310 - ( 72 * sin( kValue * x))

        radX = x.toFloat()
        radY = y.toFloat()

        invalidate()

    }
}