package com.example.dionysos

/**
 * Represents a incoming message from the safety console
 * @param eventType: e.g 0 for clock-in/clock-out
 * @param value: e.g 1 for clock-in = true
 */
class CommandModel(var eventType: Int, var value: Int)