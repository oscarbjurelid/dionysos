package com.example.dionysos

import java.time.LocalTime

/**
 * [TechnicianModel] represent a technician with his/her values
 */
class TechnicianModel {

    companion object{
        var exposureLeft = 500000
        var hazmatOn = 0
        var roomIn = 1 // Break room per default
        var shiftStart = LocalTime.now()

        fun getRoomCoefficient() : Double {
            return when (roomIn) {
                1 -> BREAK_ROOM_COEF
                2 -> REACTOR_ROOM_COEF
                3 -> CONTROL_ROOM_COEF
                else -> BREAK_ROOM_COEF
            }
        }

        fun getClothesCoefficient() : Int {
            return when (hazmatOn) {
                0 -> HAZMAT_OFF_COEF
                1 -> HAZMAT_ON_COEF
                else -> HAZMAT_OFF_COEF
            }
        }
    }
}