package com.example.dionysos

import android.content.Context
import android.content.Intent
import android.util.Log
import app.akexorcist.bluetotohspp.library.BluetoothSPP
import app.akexorcist.bluetotohspp.library.BluetoothState
/**
 * Implements [BluetoothSPP] library
 * @see [link](https://github.com/akexorcist/Android-BluetoothSPPLibrary)
 *
 */
class BluetoothHandler(context: Context) {


    //region MEMBER VARIABLES
    private val mBluetooth = BluetoothSPP(context)
    private val mCommandHandler = CommandHandler(context)
    //endregion

    //region PRIVATE MEMBER FUNCTIONS
    private fun createConnectIntent() : Intent {

        val intent = Intent()
        intent.putExtra(BluetoothState.EXTRA_DEVICE_ADDRESS, MAC_ADDRESS_SAFETY_CONSOLE)

        return intent
    }
    //endregion

    //region PUBLIC MEMBER FUNCTIONS
    fun init() : Boolean{

        try {
            if (!mBluetooth.isServiceAvailable) {
                mBluetooth.setupService()
                mBluetooth.startService(BluetoothState.DEVICE_OTHER)

                val intent = createConnectIntent()
                mBluetooth.connect(intent)


            }

        } catch (e: Exception) {
            Log.e("BluetoothHandler", "Error: $e")
            return false
        }
        return true
    }

    fun stop() {
        try {
            mBluetooth.stopService()
        } catch (e: Exception) {
            Log.e("BluetoothHandler", "Error: $e")
        }

    }

    fun send(flag: Int, value: Int = -1) {
        when (flag) {
            TIME_LEFT_INDICATION -> {
                mBluetooth.send("$3:$value$", true)
            }
            SYSTEM_WIDE_WARNING -> {
                mBluetooth.send("$4:$value$", true)
            }
        }
    }


    fun startListeningForMsg() {
        mBluetooth.setOnDataReceivedListener { _, message ->

            val cmd = MessageParser.parse(message)
            mCommandHandler.handle(cmd)

        }
    }
    //endregion

}