package com.example.dionysos


import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView

/**
 * [HistoryAdapter] inflates a [RecyclerView] consisting of data from [DBHandler]
 */
class HistoryAdapter(private val dataSet: ArrayList<ListModel> ) : RecyclerView.Adapter<HistoryAdapter.MyViewHolder>() {

    class MyViewHolder(val llHistory: ConstraintLayout) : RecyclerView.ViewHolder(llHistory)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : MyViewHolder {
            val layoutToBeReturned = LayoutInflater.from(parent.context)
                .inflate(R.layout.history_item, parent, false) as ConstraintLayout


            return MyViewHolder(layoutToBeReturned)
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.llHistory.findViewById<ImageView>(R.id.history_icon).setImageResource(dataSet[position].icon!!)
        holder.llHistory.findViewById<TextView>(R.id.history_action).text = dataSet[position].action
        holder.llHistory.findViewById<TextView>(R.id.history_time).text = dataSet[position].time
    }


    override fun getItemCount() = (dataSet.size -1)


}

