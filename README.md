<img src="https://upload.wikimedia.org/wikipedia/commons/1/11/Dionysos_satyrs_Cdm_Paris_575.jpg" width="600">

# Dionysos

The Dionysos project is trying to achieve that nuclear castatrophes like chernobyl will never happen again. 

<img src="https://scrumorg-website-prod.s3.amazonaws.com/drupal/inline-images/Scrum_and_XP.jpg" width="600">

## Table of Contents
1. [Link collection](#links)
2. [Tech](#tech)
3. [Instructions](#instructions)

## Links <a name="links"></a>
### XP
* [Extreme Programming](http://www.extremeprogramming.org/)

### SEPM Repo
* [SEPM Repo](https://github.com/zimonitrome/SEPM)
* [SEPM User Stories](https://github.com/zimonitrome/SEPM/projects/1)
* [SEPM Vision-document](https://github.com/zimonitrome/SEPM/blob/master/Vision-document.pdf)

### Dionysos Documents
* [Drive folder](https://l.messenger.com/l.php?u=https%3A%2F%2Fdrive.google.com%2Fopen%3Fid%3D1hrfNAb8gC3EVrxDuxs1fEtjc545z10Zp&h=AT1WzOHULWXscHL0AE47wGYWDFpqtxV-oEFwV6Btw7XTPx9LtO7fXJhoHc6Wmitc_BCsr2lqR8El11zcWxtQYFYhRXCOxoWruUlg5SJBzv_T0-jxke0ZpssUjqK05g)


## Tech <a name="tech"></a>

### Editor config
In Android Studio 3.5
`Settings -> Editor -> Code Style -> Enable EditorConfig support`
```
// .editorconfig
[*]
charset=utf-8
end_of_line=lf
insert_final_newline=false
indent_style=space
indent_size=4
```


### Android
* Android Studio 3.5
* Android SDK API level 28

[Coding Standard](https://kotlinlang.org/docs/reference/coding-conventions.html)

### Arduino

### Database
* [Firebase](https://firebase.google.com/)


## Instructions <a name="instructions"></a>
1. `cd folder-of-your-choice`
2. `git clone https://gitlab.com/oscarbjurelid/dionysos.git`

### Git for dummies
``` 
// Before coding
git fetch -all // Gets new data from remote repo. Dosen't modify your working files.
git pull origin "branch" // Merge new updates (from remote) with your working files.

git checkout "branchName" // Switches between existing branches.
git checkout -b "branchName" // Creates a new branch with name = "branchName" and check out to it.
git branch -u origin branchName // -u = upstream. NOTE! I'm unsure :) 

// After development
git add -A // -all, append new files to repo
git commit -m "your commit message" // "saves" changes to local repo

git push // "saves" changes to remote

```
